import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { PieChartComponent } from "./charts/pie-chart/pie-chart.component";
import { LocationChartComponent } from "./charts/location-chart/location-chart.component";
import { ZingMapComponent } from "./charts/zing-map/zing-map.component";

import { ZingchartAngularModule } from "zingchart-angular";

@NgModule({
  declarations: [
    AppComponent,
    PieChartComponent,
    LocationChartComponent,
    ZingMapComponent
  ],
  imports: [BrowserModule, ZingchartAngularModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
