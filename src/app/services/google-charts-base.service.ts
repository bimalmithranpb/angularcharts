import { Injectable } from "@angular/core";
declare var google: any;

@Injectable({
  providedIn: "root"
})
export class GoogleChartsBaseService {
  constructor() {
    google.charts.load("current", { packages: ["corechart"] });
  }

  public buildChart(data: any[], chartFunc: any, options: any): void {
    const func = (chartFunc, options) => {
      const dataTable = google.visualization.arrayToDataTable(data);
      chartFunc().draw(dataTable, options);
    };
    const callback = () => func(chartFunc, options);
    google.charts.setOnLoadCallback(callback);
  }
}
