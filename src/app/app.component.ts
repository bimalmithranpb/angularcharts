import { Component, ViewChild } from "@angular/core";
import { PieChartComponent } from "./charts/pie-chart/pie-chart.component";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  @ViewChild("pieChart1") pieChart1: PieChartComponent;
  @ViewChild("pieChart2") pieChart2: PieChartComponent;
  sideBarBig = false;

  toggleSideBar() {
    this.sideBarBig = !this.sideBarBig;
    setTimeout(() => {
      this.pieChart1.drawChart();
      this.pieChart2.drawChart();
    }, 500);
  }
}
