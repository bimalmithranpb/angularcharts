import { Component, OnInit } from "@angular/core";
import { GoogleChartsBaseService } from "src/app/services/google-charts-base.service";
declare var google: any;

@Component({
  selector: "app-location-chart",
  templateUrl: "./location-chart.component.html",
  styleUrls: ["./location-chart.component.scss"]
})
export class LocationChartComponent implements OnInit {
  data: any[] = [
    ["Country", "Popularity"],
    ["Germany", 200],
    ["United States", 300],
    ["Brazil", 400],
    ["Canada", 500],
    ["France", 600],
    ["RU", 700]
  ];
  config = {};
  elementId: string;

  divId = `geoChart_${Math.floor(Math.random() * 10000000)}`;

  constructor(private _chartService: GoogleChartsBaseService) {}

  ngOnInit(): void {
    this.drawChart();
  }

  drawChart() {
    console.log("Draw Location Chart Called", Math.random());

    this.data = [
      ["Country", "Popularity"],
      ["Germany", 200],
      ["United States", 300],
      ["Brazil", 400],
      ["Canada", 500],
      ["France", 600],
      ["RU", 700]
    ];
    const chartFunc = () => {
      return new google.visualization.GeoChart(
        document.getElementById(this.divId)
      );
    };
    const options = this.config;

    this._chartService.buildChart(this.data, chartFunc, options);
  }
}
