import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZingMapComponent } from './zing-map.component';

describe('ZingMapComponent', () => {
  let component: ZingMapComponent;
  let fixture: ComponentFixture<ZingMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZingMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZingMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
