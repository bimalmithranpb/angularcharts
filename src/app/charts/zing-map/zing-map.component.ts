import { Component, OnInit, ViewChild } from "@angular/core";
import zingchart from "zingchart";

import "zingchart/modules/zingchart-maps.min.js";
import "zingchart/modules/zingchart-maps-world-countries.min.js";

function rand(min, max) {
  return Math.round(min + (max - min) * Math.random());
}

function setResults() {
  const items = zingchart.maps.getItems("world.countries");

  const data = {};

  items.forEach(value => {
    data[value] = {
      dataValue: null,
      text: 'None'
    };
  });

  data["IND"] = {
    dataValue: 740,
    text: 'Count = 740'
  };
  data["USA"] = {
    dataValue: 1321,
    text: 'Count = 1321'
  };
  data["AUS"] = {
    dataValue: 420,
    text: 'Count = 420'
  };
  data["CHN"] = {
    dataValue: 900,
    text: 'Count = 900'
  };
  data["SWZ"] = {
    dataValue: 502,
    text: 'Count = 502'
  };

  return {
    colorScale: {
      layout: "h",
      width: 100,
      height: 10,
      margin: "auto auto 20 auto",
      item: {
        fontSize: 10,
        offsetY: -5
      },
      cursor: {
        size: 3
      },
      map: true
    },
    shapes: [
      {
        type: "zingchart.maps",
        options: {
          x: "5%",
          y: "10%",
          width: "90%",
          height: "80%",
          id: "mapp1",
          name: "world.countries",
          scale: true,
          zooming: true,
          panning: true,
          scrolling: true,
          colorScale: true,
          choropleth: {
            aspect: "gradient",
            progression: "lin",
            color: "blue",
            maxPercent: 50,
            effect: "lighten",
            mirrored: true
          },
          style: {
            controls: {
              visible: true
            },
            borderAlpha: 0.1,
            borderColor: "#000000",
            label: {
              visible: false
            },
            hoverState: {
              backgroundColor: "none",
              shadowAlpha: 0.1,
              shadowDistance: 0,
              shadow: true,
              shadowColor: "#369"
            },
            tooltip: {
              callout: true,
              calloutWidth: 16,
              calloutHeight: 8,
              borderRadius: 3,
              color: "#000000",
              backgroundColor: "#ffffff",
              padding: "10 20",
              borderWidth: 0,
              shadow: true,
              shadowColor: "#333",
              shadowAlpha: 0.7,
              fontSize: 13,
              fontWeight: "bold",
              shadowDistance: 2,
              text: "%text: %data-value"
            },
            items: data
          }
        }
      }
    ]
  };
}

@Component({
  selector: "app-zing-map",
  templateUrl: "./zing-map.component.html",
  styleUrls: ["./zing-map.component.scss"]
})
export class ZingMapComponent implements OnInit {
  @ViewChild("chart1", { static: false }) chart: any;
  title = "zing-app";
  value = 0;
  plotindex = 0;
  nodeindex = 0;
  dataset = [];
  config: zingchart.graphset = setResults();

  constructor() {}

  ngOnInit(): void {}
}
