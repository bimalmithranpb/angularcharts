import { Component, OnInit } from "@angular/core";
import { PieChartConfig } from "src/app/models/chart-configs";
import { GoogleChartsBaseService } from "src/app/services/google-charts-base.service";
declare var google: any;

@Component({
  selector: "app-pie-chart",
  templateUrl: "./pie-chart.component.html",
  styleUrls: ["./pie-chart.component.scss"]
})
export class PieChartComponent implements OnInit {
  data: any[] = [
    ["Task", "Hours per Day"],
    ["Eat", 3],
    ["Commute", 2],
    ["Watch TV", 5],
    ["Video games", 4],
    ["Sleep", 10]
  ];
  config = {
    title: "My Daily Activities at 20 years old",
    pieHole: 0,
    chartArea: { width: "80%", height: "80%" },
    animation: { startup: true },
    pieStartAngle: 0,
    tooltip: { isHtml: true, ignoreBounds: true, showColorCode: true }
  };
  elementId: string;

  divId = `pieChart_${Math.floor(Math.random() * 10000000)}`;

  constructor(private _chartService: GoogleChartsBaseService) {}

  ngOnInit(): void {
    this.drawChart();
  }

  drawChart() {
    console.log("Draw Chart Called", Math.random());

    this.data = [
      ["Task", "Hours per Day"],
      ["Eat", Math.floor(Math.random() * 100)],
      ["Commute", Math.floor(Math.random() * 100)],
      ["Watch TV", Math.floor(Math.random() * 100)],
      ["Video games", Math.floor(Math.random() * 100)],
      ["Sleep", Math.floor(Math.random() * 100)]
    ];
    const chartFunc = () => {
      return new google.visualization.PieChart(
        document.getElementById(this.divId)
      );
    };
    const options = this.config;

    this._chartService.buildChart(this.data, chartFunc, options);
  }
}
